#include "pitches.h"
#include "button.h"
#include "buzzer.h"

#define PIN_BUTTON_SELECT 3
#define PIN_BUTTON_OFF 5
#define PIN_BUTTON_SPEED 4
#define PIN_BUZZER 6

Button buttonMelodySelect(PIN_BUTTON_SELECT);
Button buttonOff(PIN_BUTTON_OFF);
Button buttonSpeed(PIN_BUTTON_SPEED);
Buzzer buzzer(PIN_BUZZER);

int notes[] = {NOTE_A4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE};
double durations[] = {8, 1, 4, 1};
int melodyLength = 4;

unsigned long speeds[] = {25, 50, 100, 200, 400, 800};
int currentSpeed = 2;
int speedsLength = 6;



// and the second melody
int notes2[] = {NOTE_A5, NOTE_D7, NOTE_E7, NOTE_F7, NOTE_G7, NOTE_F7, NOTE_E7, NOTE_D7, NOTE_C7, NOTE_A5};
double durations2[] = {0, 6, 6, 4, 4, 4, 4, 4, 4, 6, 8};
 int melodyLength2 = 10;

int currentMelody = 0;
int meloodiesLenght = 2;

void setup()
{
    buzzer.setMelody(notes, durations, melodyLength);
}

void loop()
{
    buzzer.playSound();

if (buttonOff.wasPressed())

{      
  buzzer.turnSoundOff();
}


    if (buttonMelodySelect.wasPressed())
    {
      if (currentMelody == 0)
    
    {
        buzzer.setMelody(notes, durations, melodyLength);
        buzzer.turnSoundOn();
    }
 if (currentMelody == 1)
   {
        buzzer.setMelody(notes2, durations2, melodyLength2);
        buzzer.turnSoundOn();
          }
          }

    if (buttonSpeed.wasPressed())
    {
        currentSpeed = (currentSpeed + 1)%speedsLength;
        buzzer.setNoteDuration(speeds[currentSpeed]);
    }
}
